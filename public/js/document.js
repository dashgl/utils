"use strict";

const AtlasTools = (function() {

	this.MEM = {
		images : []
	};

	this.DOM = {
		add : document.getElementById('AtlasTools.add'),
		file : document.getElementById('AtlasTools.file'),
		list : document.getElementById('AtlasTools.list'),
		image : document.getElementById('AtlasTools.image'),
		json : document.getElementById('AtlasTools.json'),
		preview : document.getElementById('AtlasTools.preview'),
		size : document.getElementById('AtlasTools.size'),
		length : document.getElementById('AtlasTools.length')
	};

	this.EVT = {
		handleAddClick : evt_handleAddClick.bind(this),
		handleFileChange : evt_handleFileChange.bind(this),
		handleTrashClick : evt_handleTrashClick.bind(this),
		handleCanvasDownload : evt_handleCanvasDownload.bind(this),
		handleJsonDownload : evt_handleJsonDownload.bind(this)
	};

	this.API = {
		addFiles : api_addFiles.bind(this),
		readFile : api_readFile.bind(this),
		generateAtlas : api_generateAtlas.bind(this),
		uuidv4 : api_uuidv4.bind(this)
	};

	init.apply(this);
	return this;

	function init() {
		
		this.DOM.add.addEventListener("click", this.EVT.handleAddClick);
		this.DOM.file.addEventListener("change", this.EVT.handleFileChange);
		this.DOM.json.addEventListener("click", this.EVT.handleJsonDownload);
		this.DOM.image.addEventListener("click", this.EVT.handleCanvasDownload);

	}

	function evt_handleAddClick() {
		
		this.DOM.file.click();

	}

	function evt_handleFileChange(evt) {

		let files = evt.target.files || evt.dataTransfer.files;
		this.API.addFiles(files);

	}

	function evt_handleTrashClick(evt) {

		let uuid = evt.target.getAttribute("data-uuid");
		
		for(let i = 0; i < this.DOM.list.children.length; i++) {
			if(this.DOM.list.children[i].tagName !== "LI") {
				continue;
			}

			let li = this.DOM.list.children[i];
			if(li.getAttribute("data-uuid") !== uuid) {
				continue;
			}
			
			this.DOM.list.removeChild(li);
			break;
		}
		
		let index = 0;
		for(let i = 0; i < this.DOM.list.children.length; i++) {
			if(this.DOM.list.children[i].tagName !== "LI") {
				continue;
			}

			let li = this.DOM.list.children[i];
			index++;
			let num = index.toString();
			while(num.length < 3) {
				num = "0" + num;
			}
			
			let table = li.children[0];
			table.rows[0].cells[0].textContent = num;

		}

		for(let i = 0; i < this.MEM.images.length; i++) {
			if(this.MEM.images[i].uuid !== uuid) {
				continue;
			}
			this.MEM.images.splice(i, 1);
			break;
		}
		
		this.API.generateAtlas();

	}

	function evt_handleCanvasDownload() {

		let savable = this.MEM.canvas.toDataURL();
		let index = savable.indexOf(',') + 1
		let base = savable.substr(index);
		const byteCharacters = atob(base);

		const byteNumbers = new Array(byteCharacters.length);
		for (let i = 0; i < byteCharacters.length; i++) {
			byteNumbers[i] = byteCharacters.charCodeAt(i);
		}

		const byteArray = new Uint8Array(byteNumbers);
		const blob = new Blob([byteArray], {type: "image/png"});
		saveAs(blob, "atlas.png");

	}

	function evt_handleJsonDownload() {

		let blob = new Blob([this.MEM.json], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "atlas.json");

	}

	async function api_addFiles(files) {
		
	
		for(let i = 0; i < files.length; i++) {

			let name = files[i].name.split(".").shift();
			let img = await this.API.readFile(files[i]);
			img.name = name;
			img.uuid = this.API.uuidv4();

			this.MEM.images.push(img);

			let li = document.createElement("li");
			let table = document.createElement("table");
			let row = table.insertRow();
			li.setAttribute("data-uuid", img.uuid);
			
			let cell;
			cell = row.insertCell();
			cell.setAttribute("class", "num");
			let num = this.MEM.images.length.toString();
			while(num.length < 3) {
				num = "0" + num;
			}
			cell.textContent = num;

			cell = row.insertCell();
			cell.textContent = img.name;

			cell = row.insertCell();
			cell.setAttribute("class", "width");
			cell.textContent = img.width;

			cell = row.insertCell();
			cell.setAttribute("class", "width");
			cell.textContent = img.width;

			cell = row.insertCell();
			cell.setAttribute("class", "trash");
			let icon = document.createElement("i");
			icon.setAttribute("class", "fas fa-trash");
			cell.appendChild(icon);
			icon.setAttribute("data-uuid", img.uuid);
			icon.addEventListener("click", this.EVT.handleTrashClick);

			li.appendChild(table);
			this.DOM.list.appendChild(li);

		}

		this.API.generateAtlas();

	}

	function api_readFile(file) {

		return new Promise( (resolve, reject) => {
			
			let reader = new FileReader();

			reader.onload = () => {
				
				let image = new Image();
				image.src = reader.result;
				image.onload = () => {
					resolve(image);
				}

			}

			reader.readAsDataURL(file);

		});

	}

	function api_generateAtlas() {

		let packer = new ImagePacker();
		packer.pack(this.MEM.images);
		let src = packer.canvas.toDataURL();
		this.DOM.preview.style.backgroundImage = 'url(' + src + ')';
		
		this.MEM.canvas = packer.canvas;
		this.MEM.json = JSON.stringify(packer.output, null, 4);
		
		this.DOM.size.textContent = this.MEM.canvas.width + " x " + this.MEM.canvas.height;
		this.DOM.length.textContent = this.MEM.json.length + " bytes";

	}


	function api_uuidv4() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		});
	}

}).apply({});
