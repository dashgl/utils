let ImagePacker = function(padding) {
	
	this.padding = padding || 0;
	this.padding = 0;
	this.coords = [];
	this.canvas = document.createElement("canvas");
	this.ctx = this.canvas.getContext("2d");
	this.output = {}

}

ImagePacker.prototype = {

	constructor : ImagePacker,

	pack : function(imgs) {
	
		this.createAltas(imgs);
		
		// Render the images to the canvas

		let sx, sy, sWidth, sheight, dx, dy, dWidth, dHeight;

		for(let i = 0; i < this.coords.length; i++) {
			
			let src = this.coords[i].image;
			this.ctx.drawImage(src, this.coords[i].x, this.coords[i].y);

			// Create new canvas for mirroring
			
			if(this.padding) {

				src = document.createElement("canvas");
				src.width = this.coords[i].width;
				src.height = this.coords[i].height;

				let ctx = src.getContext("2d");

				// Flip canvas vertically

				ctx.save();
				ctx.scale(1, -1);
				ctx.translate(0, -src.height);
				ctx.drawImage(this.coords[i].image, 0, 0);
				ctx.restore();
			
				// Draw Mirrored top
			
				sx = 0;
				sy = src.height - this.padding;
				sWidth = src.width;
				sHeight = this.padding;
				dx = this.coords[i].x;
				dy = this.coords[i].y - this.padding;
				dWidth = src.width;
				dHeight = this.padding;
				this.ctx.drawImage(src,sx,sy,sWidth,sHeight, dx, dy, dWidth, dHeight);

				// Draw Mirrored bottom
			
				sx = 0;
				sy = 0;
				sWidth = src.width;
				sHeight = this.padding;
				dx = this.coords[i].x;
				dy = this.coords[i].y + src.height;
				dWidth = src.width;
				dHeight = this.padding;
				this.ctx.drawImage(src,sx,sy,sWidth,sHeight, dx, dy, dWidth, dHeight);
			
				// Clear canvas and flip horizontally
			
				ctx.clearRect(0,0,src.width,src.height);
				ctx.save();
				ctx.scale(-1, 1);
				ctx.translate(-src.width, 0);
				ctx.drawImage(this.coords[i].canvas, 0, 0);
				ctx.restore();

				// Draw Mirrored Right
			
				sx = src.width - this.padding;
				sy = 0;
				sWidth = this.padding;
				sHeight = src.height;
				dx = this.coords[i].x - this.padding;
				dy = this.coords[i].y;
				dWidth = this.padding;
				dHeight = src.height;
				this.ctx.drawImage(src,sx,sy,sWidth,sHeight, dx, dy, dWidth, dHeight);

				// Draw Mirrored Left
			
				sx = 0;
				sy = 0;
				sWidth = this.padding;
				sHeight = src.height;
				dx = this.coords[i].x + src.width;
				dy = this.coords[i].y;
				dWidth = this.padding;
				dHeight = src.height;
				this.ctx.drawImage(src,sx,sy,sWidth,sHeight, dx, dy, dWidth, dHeight);
			}

			this.output[this.coords[i].name] = [
				this.coords[i].x / this.canvas.width,
				this.coords[i].y / this.canvas.height,
				this.coords[i].width / this.canvas.width,
				this.coords[i].height / this.canvas.height
			]

		}
		
	},

	createAltas : function(imgs) {

		for(let i = 0; i < imgs.length; i++) {
			imgs[i].size = imgs[i].width * imgs[i].height;
		}

		let side = 0;
		for(let i = 0; i < imgs.length; i++) {
			side += imgs[i].size;
		}

		side = Math.sqrt(side);

		imgs.sort((a, b) => {
			return b.size - a.size;
		});
		
		let power = 0;
		let mipSize = 0;

		while(mipSize < side) {
			power++;
			mipSize = 1 << power;
		}

		this.canvas.width = mipSize;
		this.canvas.height = mipSize;
		
		this.coords = [];
		this.min_y = 0;
		this.max_y = 0;

		for(let i = 0; i < imgs.length; i++) {
			let area = this.findSquare(0, this.min_y, imgs[i].width, imgs[i].height);
		
			if(area.max_y > this.max_y) {
				this.max_y = area.max_y;
			}

			area.x = area.min_x + this.padding;
			area.y = area.min_y + this.padding;
			area.width = imgs[i].width;
			area.height = imgs[i].height;
			area.name = imgs[i].name;
			area.image = imgs[i];
			this.coords.push(area);
		}

		if(this.max_y < this.canvas.height / 2) {
			let half = document.createElement("canvas");
			half.width = this.canvas.width;
			half.height = this.canvas.height / 2;
			let htx = half.getContext("2d");
			this.canvas = half;
			this.ctx = htx;
		}

	},


	findSquare: function (x, y, width, height) {
		
		let min_x = x;
		let min_y = y;
		let max_x = x + width + (this.padding*2);
		let max_y = y + height + (this.padding*2);

		// Set found by default

		let found;
		
		while(!found) {
			
			found = true;

			// Check against existing coordinates

			for(let i = 0; i < this.coords.length; i++) {
		
				let bool = (
					max_x <= this.coords[i].min_x || 
				    max_y <= this.coords[i].min_y || 
				    min_x >= this.coords[i].max_x || 
				    min_y >= this.coords[i].max_y
  				) 

				if(!bool){
					found = false;
					break;
				}

			}
		
			// Check to make sure coords are inside canvas
	
			if(!found) {

				min_x += width;
				max_x += width;

				if(max_x > this.canvas.width) {
					min_x = 0;
					max_x = min_x + width + (this.padding*2);

					min_y += height;
					this.min_y = min_y;
					max_y = min_y + height + (this.padding*2);

				}

			}

		}

		// If a space is found then return it

		return {
			min_x : min_x,
			max_x : max_x,
			min_y : min_y,
			max_y : max_y
		}
		
	}

}
